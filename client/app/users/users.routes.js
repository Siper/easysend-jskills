'use strict';

export default function routes($stateProvider) {
  'ngInject';

  $stateProvider
  .state('user', {
    url: '/users/:id',
    resolve: {
      user: ($http) => {
        return $http.get('/api/users/123')
          .then(response => {
            return response.data;
          });
      }
    },
    template: `<div class="container">
                <div class="row">
                  <div class="col-lg-12">
                    <h1 class="page-header">User: {{ user.first_name }} {{ user.last_name }}</h1>
                  </div>
                  <div class="col-lg-8">
                    <user data="$resolve.user"></user>
                  </div>
                </div>
              </div>`
  })

  .state('usersList', {
        url: "/users",
        resolve: {
            users: ($http) => {
            return $http.get('/api/users')
                .then(response => {
                return response.data;
                });
            }
        },
        controller: ['$scope', 'Modal', function($scope, Modal) {
          $scope.users = $scope.$resolve.users;
          
          $scope.$on('removeUser', function (ev, user) {
              var index = $scope.users.indexOf(user)

              Modal.confirm.delete(function(){
                $scope.users.splice(index, 1);
              })(user.first_name + " " + user.last_name);
          });
        }],
        template: require('./usersList.html')
    });
}
