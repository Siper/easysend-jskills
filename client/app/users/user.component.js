import angular from 'angular';
import uiRouter from 'angular-ui-router';
import routing from './users.routes';

export class UserController {

  user = {};

  /*@ngInject*/
  constructor($scope) {
    this.user = this.data;
    this._$scope = $scope;
  }

  toggleStar() {
    if (this.user.star) {
      this.user.star = false;
    } else {
      this.user.star = true;
    }
  }

  remove() {
    this._$scope.$emit('removeUser',this.user);
  }

}
export default angular.module('jskillsApp.users', [uiRouter])
  .config(routing)
  .filter('phone', function () {
    return function (item) {
        if (item) {
          return item.substring(0,3) + "-" + item.substring(3,6) + "-" + item.substring(6,9);
        }
    };
  })
  .component('user', {
    template: require('./details.html'),
    controller: UserController,
    bindings: {
      data: '<',
      index: '='
    }
  })
  .name;
